
// Función Main

import java.util.Scanner;

public class Main {
    public static void main (String[] args){

        Scanner scanner = new Scanner (System.in);
        System.out.println("MENU DE OPCIONES \n");
        System.out.println("1.- Suma \n");
        System.out.println("2.- Resta \n");
        System.out.println("3.- Multiplicación \n");
        System.out.println("4.- División \n");
        System.out.println("5.- Salir \n");

        System.out.println("¿Qué opción deseas ejecutar? \n"); //Recogemos opcion del usuario
        int opcionUsuario = scanner.nextInt();

        System.out.println("Introduce un número: \n"); //Recogemos numero que introduce el usuario
        int primerNumero = scanner.nextInt();

        System.out.println("Introduce otro número: \n");
        int segundoNumero = scanner.nextInt();

        System.out.println("\n");
        int resultado = primerNumero + segundoNumero;
        System.out.println("El resultado de la operación es " + resultado);
        System.out.println("**********************************");

        System.out.println("Introduce un número: \n"); //Recogemos numero que introduce el usuario
        int primerNumero_resta = scanner.nextInt();

        System.out.println("Introduce otro número: \n");
        int segundoNumero_resta = scanner.nextInt();

        System.out.println("\n");
        int resultado_resta = primerNumero_resta - segundoNumero_resta;
        System.out.println("El resultado de la operación es " + resultado_resta);
        System.out.println("**********************************");
    }

}
